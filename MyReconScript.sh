#!/bin/bash

if [ "$1" == "-h" ]; then
###   echo "Usage: `basename $0` exemple.com [nmap|dirb|whatweb|crt|nikto|wget]"
   echo "Usage: $(basename "$0") example.com [nmap|dirb|whatweb|crt|nikto|wget]"
   exit 0
fi

### Prérequis : sudo apt install jq
DATE=`date +"%Y-%m-%d"`
TODAY=$(date)
echo "This scan was created on : $TODAY"
DOMAIN=$1
echo "Going to scan : $DOMAIN"
DIRECTORY="${DOMAIN}_recon"

if [ ! -d $DIRECTORY ];
then
	echo "Creating directory : $DIRECTORY."
	mkdir "$DIRECTORY"
fi

nmap_scan()
{
	nmap -sC -sV -p- -O "$DOMAIN" > "$DIRECTORY/nmap_$DATE"
###	nmap "$DOMAIN" > "$DIRECTORY/nmap_$DATE"
	echo "The results of nmap scan are stored in $DIRECTORY/nmap."
}
dirb_scan()
{
	dirb "http://$DOMAIN" -f -N 404 -S >  "$DIRECTORY/dirb_$DATE"
	echo "The results of dirb scan are stored in $DIRECTORY/dirb."
}
whatweb_scan()
{
	whatweb -a 3 --color=never "$DOMAIN" >  "$DIRECTORY/whatweb_$DATE"
	echo "The results of whatweb scan are stored in $DIRECTORY/whatweb."
}
crt_scan()
{
	curl "https://crt.sh/?q=$DOMAIN&output=json"  | jq -r ".[] | .name_value" | sort -u > "$DIRECTORY/crt_$DATE"
	echo "The results of cert parsing is stored in $DIRECTORY/crt."
}
nikto_scan()
{
	nikto -h "http://$DOMAIN" >  "$DIRECTORY/nikto_$DATE"
	echo "The results of nmap scan are stored in $DIRECTORY/nikto."
}
### Wapiti
### sudo apt-get install wapiti
### wapiti http://www.site.fr/
### sudo wapiti -u http://exemple.com -o exemple.com -f txt
wget_files()
{
	### Wget robots.txt
	wget "https://$DOMAIN/robots.txt" -O "$DIRECTORY/robots_$DATE"
	echo "Wget robots.txt stored in $DIRECTORY/robots.txt."
	### Wget sitemap.xml
	wget "https://$DOMAIN/sitemap.xml" -O "$DIRECTORY/sitemap_$DATE"
	echo "Wget sitemap.xml stored in $DIRECTORY/sitemap.xml."
	#### Wget security.txt
	wget "https://$DOMAIN/security.txt" -O "$DIRECTORY/security_$DATE"
	echo "Wget security.txt stored in $DIRECTORY/security.txt."
}

case $2 in
   "nmap")      nmap_scan ;;
   "dirb")      dirb_scan ;;
   "whatweb")   whatweb_scan ;;
   "crt")       crt_scan ;;
   "nikto")     nikto_scan ;;
   "wget")      wget_files ;;
   *)           # Tous les scans par défaut
		nmap_scan
		dirb_scan
		whatweb_scan
		crt_scan
		nitko_scan
		wget_files
		;;
esac

